﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    [HideInInspector]
    public float breakDistance = 0;

    [HideInInspector]
    public float[] borderSize = { 0, 0, 0, 0 };

    [HideInInspector]
    public GameObject player;

    private bool isTracking = true;

    private void FixedUpdate()
    {
        if (transform.position.x < borderSize[0] || transform.position.x > borderSize[1] || transform.position.y > borderSize[2] || transform.position.y < borderSize[3])
        {
            Destroy(gameObject);
        }

        if(player != null)
        {
            Rigidbody2D rb2d = GetComponent<Rigidbody2D>();
            Vector2 velocity = rb2d.velocity;
            float speed = velocity.magnitude;

            if (isTracking)
            {
                rb2d.velocity = ((Vector2)(player.transform.position - transform.position)).normalized * speed;
            }
            if(Vector2.Distance(transform.position, player.transform.position) < breakDistance)
            {
                isTracking = false;
                GetComponent<SpriteRenderer>().color = Color.red;
            }
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
