﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [Header("General Options")]
    public GameObject bulletFab;
    

    [Header("Bullet Values")]
    public float DefaultDelay = 0.2f;
    public float DefaultBulletSpeed = 10;
    public float startRotation = 0;
    public float rotationSpeed = 7;
    public float breakDistance = 3;

    [Header("Bullet Options")]
    public bool tracking = true;
    public bool splitShot = false;

    [HideInInspector]
    public RectTransform border;
    [HideInInspector]
    public GameObject player;

    private float delay;
    private float timePassed;

    private float bulletSpeed;

    private double currentAngle = 0;
    private Vector2 currentVel = new Vector2(0, 0);

    private float[] borderSize = { 0, 0, 0, 0 };

    void Start()
    {
        resetEnemy();

        border = transform.parent.GetComponent<EnemyControllerBehaviour>().border;
        player = transform.parent.GetComponent<EnemyControllerBehaviour>().player;

        borderSize[0] = -(border.rect.width / 2) + border.transform.position.x;
        borderSize[1] = (border.rect.width / 2) + border.transform.position.x;
        borderSize[2] = (border.rect.height / 2) + border.transform.position.y;
        borderSize[3] = -(border.rect.height / 2) + border.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed >= delay)
        {
            if (tracking)
            {
                shootBulletTracking();
            }
            else
            {
                shootBulletRotating();
            }
            timePassed = 0;
        }
    }

    public void resetEnemy()
    {
        timePassed = 0;
        currentAngle = startRotation;
        currentVel = new Vector2(0, 0);
        delay = DefaultDelay;
        bulletSpeed = DefaultBulletSpeed;
    }

    public void difficultyUp()
    {
        delay /= 1.2f;
        bulletSpeed *= 1.2f;
    }

    public void difficultyDown()
    {
        delay *= 1.2f;
        bulletSpeed /= 1.2f;
    }

    void shootBulletTracking()
    {
        player = transform.parent.GetComponent<EnemyControllerBehaviour>().player;
        GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation, transform.parent);
        bullet.GetComponent<BulletBehaviour>().borderSize = borderSize;
        bullet.GetComponent<BulletBehaviour>().breakDistance = breakDistance;
        bullet.GetComponent<BulletBehaviour>().player = player;
        //homing
        Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
        Vector2 velocity = new Vector2();
        if (player != null)
        {
            velocity = ((Vector2)(player.transform.position - bullet.transform.position)).normalized * bulletSpeed;
            bulletRb2d.velocity = velocity;
        }
        else
        {
            Destroy(bullet);
        }
    }

    void shootBulletRotating()
    {
        GameObject bullet = Instantiate(bulletFab, transform.position, transform.rotation, transform.parent);
        bullet.GetComponent<BulletBehaviour>().borderSize = borderSize;
        Rigidbody2D bulletRb2d = bullet.GetComponent<Rigidbody2D>();
        currentVel.x = (float)(-bulletSpeed * Math.Cos(Math.PI * currentAngle / 180));
        currentVel.y = (float)(-bulletSpeed * Math.Sin(Math.PI * currentAngle / 180));
        bulletRb2d.velocity = currentVel;
        if (splitShot)
        {
            GameObject bullet2 = Instantiate(bulletFab, transform.position, transform.rotation, transform.parent);
            bullet2.GetComponent<BulletBehaviour>().borderSize = borderSize;
            Rigidbody2D bulletRb2d2 = bullet2.GetComponent<Rigidbody2D>();
            bulletRb2d2.velocity = currentVel * -1;
        }
        currentAngle += rotationSpeed;
    }
}
