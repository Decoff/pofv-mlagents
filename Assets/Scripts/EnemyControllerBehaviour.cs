﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerBehaviour : MonoBehaviour
{
    [Header("Attributes")]
    public GameObject player;
    public RectTransform border;

    private List<GameObject> enemies = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        player = transform.parent.Find("Agent").gameObject;
        for(int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).tag == "Enemy")
            {
                enemies.Add(transform.GetChild(i).gameObject);
            }
        }
    }

    private void FixedUpdate()
    {
        player = transform.parent.Find("Agent").gameObject;
    }

    public void randomizeEnemy()
    {
        for(int i = 0; i < enemies.Count; i++)
        {
            float random = Random.Range(0f, 1f);
            if(random > 0.5f)
            {
                enemies[i].GetComponent<EnemyBehaviour>().resetEnemy();
                enemies[i].SetActive(true);
            }
            else
            {
                enemies[i].SetActive(false);
            }
        }
        int enemy = Random.Range(0, enemies.Count);
        enemies[enemy].GetComponent<EnemyBehaviour>().resetEnemy();
        enemies[enemy].SetActive(true);
    }

    public void destroyBullet()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).tag == "Bullet")
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }
}
