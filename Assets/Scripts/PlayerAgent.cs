﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine.UI;

public class PlayerAgent : Agent
{
    [Header("Player Values")]
    public RectTransform border;
    public RectTransform playerBorder;
    public GameObject enemyController;
    public Text fitnessText;
    public float speed = 10;

    private Rigidbody2D rb2d;
    private Vector2 initPosition;

    private float playerSizeX;
    private float playerSizeY;

    // left, right, up, down
    private float[] areaBorder = { 0, 0, 0, 0 };

    private float[] playerAreaBorder = { 0, 0, 0, 0 };

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        initPosition = transform.position;
        playerSizeX = GetComponent<SpriteRenderer>().bounds.size.x;
        playerSizeY = GetComponent<SpriteRenderer>().bounds.size.y;

        areaBorder[0] = -(border.rect.width / 2) + border.position.x + playerSizeX / 2;
        areaBorder[1] = (border.rect.width / 2) + border.position.x - playerSizeX / 2;
        areaBorder[2] = (border.rect.height / 2) + border.position.y - playerSizeY / 2;
        areaBorder[3] = -(border.rect.height / 2) + border.position.y + playerSizeY / 2;

        playerAreaBorder[0] = -(playerBorder.rect.width / 2) + playerBorder.position.x;
        playerAreaBorder[1] = (playerBorder.rect.width / 2) + playerBorder.position.x;
        playerAreaBorder[2] = (playerBorder.rect.height / 2) + playerBorder.position.y;
        playerAreaBorder[3] = -(playerBorder.rect.height / 2) + playerBorder.position.y;
        //Debug.LogFormat("{0};{1};{2};{3}", leftBorder, rightBorder, upBorder, downBorder);
    }

    private void FixedUpdate()
    {
        if (isInsideBorder(playerAreaBorder))
        {
            //Debug.Log("INSIDE");
            AddReward(Time.fixedDeltaTime);
        }
        else
        {
            AddReward(-Time.fixedDeltaTime);
        }
        fitnessText.text = GetCumulativeReward().ToString();
    }

    bool isInsideBorder(float[] border)
    {
        // left, right, up, down
        Vector2 position = transform.position;
        if(position.x >= border[0] && position.x <= border[1] && position.y <= border[2] && position.y >= border[3])
        {
            return true;
        }
        return false;
    }

    void clampPlayerMovement()
    {
        Vector2 position = transform.position;

        position.x = Mathf.Clamp(position.x, areaBorder[0], areaBorder[1]);
        position.y = Mathf.Clamp(position.y, areaBorder[3], areaBorder[2]);

        transform.position = position;
    }

    public override void OnEpisodeBegin()
    {
        enemyController.GetComponent<EnemyControllerBehaviour>().destroyBullet();
        enemyController.GetComponent<EnemyControllerBehaviour>().randomizeEnemy();
        transform.position = initPosition;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveY = actions.ContinuousActions[1];

        //Debug.LogFormat("{0};{1}", moveX, moveY);

        rb2d.velocity = new Vector2(moveX, moveY) * speed;
        clampPlayerMovement();
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousAction = actionsOut.ContinuousActions;
        continuousAction[0] = Input.GetAxis("Horizontal");
        continuousAction[1] = Input.GetAxis("Vertical");
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy" || collision.tag == "Bullet")
        {
            AddReward(-10);
            EndEpisode();
        }
    }
}
